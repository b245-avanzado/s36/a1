const express = require("express");
const router = express.Router();

const taskController = require("../Controllers/taskControllers.js");



/*
 Routes
*/

// Route for getAll
router.get("/", taskController.getAll);

// Route for createTask
router.post("/addTask", taskController.createTask);

// Route for deteleTask
router.delete("/deleteTasks/:id", taskController.deleteTask);

// Route for getOne
router.get("/:id", taskController.getOne);

// Route for updateStatus
router.put("/:id/:newStatus", taskController.updateStatus);








module.exports = router;