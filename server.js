const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./Routes/taskRoute.js");

const app = express();
const port = 3001;


	// MongoDB connection
	mongoose.connect("mongodb+srv://admin:admin@batch245-avanzado.ixvkr4o.mongodb.net/s35-discussion?retryWrites=true&w=majority",
		{
			// Allows us to avoid any current and future errors while connecting to MongoDB
			useNewUrlParser: true,
			useUnifiedTopology: true

		})

	// Check connection

	let db = mongoose.connection;

	// error catcher
	db.on("error", console.error.bind(console, "Connection Error!"))

	// Confirmation of the connection
	db.once("open", () => console.log("We are now connected to the cloud!"))


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// routing

app.use("/tasks", taskRoute)







app.listen(port, () => console.log(`Server is running at port ${port}!`));

/*
	Separation of concerns:
	model > controller > routes > server/application
		1. Model should be connected to the Controller.
			- taskController.js needs objects declared in Model(task.js) therefore require Model in taskController.js
		2. Controller should be connected the Routes.
			- Routes(taskRoute.js) needs functions declared in Controller(taskController.js) therefore require Controller in Routes
		3. Route should be connected to the server/application.
			- Server(server.js) needs routes(get, post, put, delete methods) in Route(taskRoute.js) therefore require Route in server.
*/